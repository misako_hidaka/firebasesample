package jp.co.bravetechnolocy.android.firebasesample;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import jp.co.bravetechnolocy.android.firebasesample.model.Post;
import jp.co.bravetechnolocy.android.firebasesample.util.FileUtil;

import static android.R.attr.data;
import static android.R.attr.key;
import static android.app.Activity.RESULT_OK;

@EActivity(R.layout.activity_create_board)
public class CreateBoardActivity extends AppCompatActivity {
    @ViewById(R.id.toolbar)
    Toolbar mToolbar;
    @ViewById(R.id.iv_create_image_thumb)
    ImageView mImageView;
    @ViewById(R.id.et_create_text)
    EditText mEditText;
    @ViewById(R.id.fab)
    FloatingActionButton mFloatingActionButton;

    private DatabaseReference mDatabase;
    private FirebaseAuth mFirebaseAuth;
    private Uri mUri;
    StorageReference mStorageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mStorageReference = FirebaseStorage.getInstance().getReferenceFromUrl(getString(R.string.storage_url));
    }

    @AfterViews
    public void initialize() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void onClickImageButton(View view) {
        showPhotoSelector();
    }

    private void showPhotoSelector() {
        String[] items = {getString(R.string.camera), getString(R.string.gallery)};
        new AlertDialog.Builder(this)
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) { //カメラ
                            showCamera();
                        } else {    //ギャラリー
                            showGallery();
                        }
                    }
                }).show();
    }

    private void showGallery() {
        Intent intentGallery;
        if (Build.VERSION.SDK_INT < 19) {
            intentGallery = new Intent(Intent.ACTION_GET_CONTENT);
            intentGallery.setType("image/*");
        } else {
            intentGallery = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intentGallery.addCategory(Intent.CATEGORY_OPENABLE);
            intentGallery.setType("image/jpeg");
        }
        intentGallery.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intentGallery, 0);
    }

    private void showCamera() {
        //カメラ起動Intentの用意
        String photoName = System.currentTimeMillis() + ".jpg";
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.Images.Media.TITLE, photoName);
        contentValues.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");

        File pathExternalPublicDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File capturedFile = new File(pathExternalPublicDir, photoName);
        mUri = Uri.fromFile(capturedFile);

        //この処理だと不要なファイルが出来るから使わない
        //mUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, mUri);
        startActivityForResult(intentCamera, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri resultUri = (data != null && data.getData() != null ? data.getData() : mUri);
            if (resultUri == null) {
                mUri = null;
                return;
            }
            mUri = resultUri;
            Picasso.with(this).load(mUri).into(mImageView);
        } else {
            mUri = null;
        }
    }


    public void onClickSaveButton(View view) {
        if (mUri != null) {
            StorageReference mountainsRef = mStorageReference.child(System.currentTimeMillis() + ".jpg");
            UploadTask uploadTask = mountainsRef.putFile(mUri);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    uploadText(downloadUrl);
                }
            });
        } else {
            uploadText(null);
        }

    }

    private void uploadText(Uri downloadUrl) {
        String key = mDatabase.child("posts").push().getKey();
        Post post = new Post(mFirebaseAuth.getCurrentUser().getUid(), mFirebaseAuth.getCurrentUser().getDisplayName(), mEditText.getText().toString(), downloadUrl == null ? "" : downloadUrl.toString());
        Map<String, Object> postValues = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posts/" + key, postValues);
        childUpdates.put("/user-posts/" + mFirebaseAuth.getCurrentUser().getUid() + "/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // いつものUPナビゲーションの処理
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
