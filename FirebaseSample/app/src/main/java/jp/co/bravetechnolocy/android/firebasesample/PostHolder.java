package jp.co.bravetechnolocy.android.firebasesample;

import android.support.v4.text.TextUtilsCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class PostHolder extends RecyclerView.ViewHolder {
    View mView;

    public PostHolder(View itemView) {
        super(itemView);
        mView = itemView;
    }

    public void setImage(String path) {
        ImageView field = (ImageView) mView.findViewById(R.id.iv_board);
        if (TextUtils.isEmpty(path)) {
            field.setImageDrawable(null);
        } else {
            Picasso.with(mView.getContext()).load(path).into(field);

        }
    }

    public void setName(String name) {
        TextView field = (TextView) mView.findViewById(R.id.tv_name);
        field.setText(name);
    }

    public void setText(String text) {
        TextView field = (TextView) mView.findViewById(R.id.tv_text);
        field.setText(text);
    }
}