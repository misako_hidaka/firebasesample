package jp.co.bravetechnolocy.android.firebasesample;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.text.TextUtilsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import jp.co.bravetechnolocy.android.firebasesample.adapter.RealtimeAdapter;
import jp.co.bravetechnolocy.android.firebasesample.model.Post;

import static jp.co.bravetechnolocy.android.firebasesample.R.id.fab;

@EActivity(R.layout.activity_board)
public class BoardActivity extends AppCompatActivity {

    @ViewById(R.id.toolbar)
    Toolbar mToolbar;
    @ViewById(R.id.fab)
    FloatingActionButton mFloatingActionButton;
    @ViewById(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @ViewById(R.id.iv_background)
    ImageView mBackgroundIv;

    private DatabaseReference mDatabase;
    private RealtimeAdapter mAdapter;
    FirebaseRemoteConfig mFirebaseRemoteConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    }

    @AfterViews
    public void initialize() {
        //    long cacheExpiration = 3600; // 1 hour in seconds.
        setSupportActionBar(mToolbar);
        mFirebaseRemoteConfig.fetch(0)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // Once the config is successfully fetched it must be activated before newly fetched
                            // values are returned.
                            mFirebaseRemoteConfig.activateFetched();
                        } else {
                        }
                        String backgroundColor = mFirebaseRemoteConfig.getString("board_background");
                        Log.d("BoardActivity", "background=" + backgroundColor);
                        if (TextUtils.isEmpty(backgroundColor)) {
                            backgroundColor = "#FFFFFF";
                        }
                        mBackgroundIv.setBackgroundColor(Color.parseColor(backgroundColor));
                    }
                });

        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BoardActivity.this, CreateBoardActivity_.class);
                startActivity(intent);
            }
        });

        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        mAdapter = new RealtimeAdapter<Post, PostHolder>(Post.class, R.layout.list_item_board, PostHolder.class, mDatabase.child("posts")) {
            @Override
            public void populateViewHolder(PostHolder chatMessageViewHolder, Post chatMessage, int position) {
                chatMessageViewHolder.setName(chatMessage.getAuthor());
                chatMessageViewHolder.setText(chatMessage.getBody());
                chatMessageViewHolder.setImage(chatMessage.getImage());
            }
        };
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAdapter.cleanup();
    }
}
