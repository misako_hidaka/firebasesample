package jp.co.bravetechnolocy.android.firebasesample.util;

/**
 * Created by MisakoGunjima on 2016/07/06.
 */

public class FileUtil {
    public static String getSuffix(String fileName) {
        if (fileName == null) return null;
        int point = fileName.lastIndexOf(".");
        if (point != -1) {
            return fileName.substring(point + 1);
        }
        return null;
    }
}
